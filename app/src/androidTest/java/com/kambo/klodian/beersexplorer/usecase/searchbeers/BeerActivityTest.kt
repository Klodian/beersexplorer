package com.kambo.klodian.beersexplorer.usecase.searchbeers


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.widget.LinearLayout
import com.kambo.klodian.beersexplorer.R
import com.kambo.klodian.beersexplorer.usecase.searchbeers.testutil.TestUtils.Companion.setTextInTextView
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers.`is`
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class
BeerActivityTest {

    @get:Rule
    val testRules = ActivityTestRule<BeerActivity>(BeerActivity::class.java)
    private var activity: BeerActivity? = null

    @Before
    fun setUp() {
        activity = testRules.activity
    }

    @After
    fun tearDown() {
        activity = null
    }

    @Test
    fun testStartBeerActivity() {
        val view = activity?.findViewById<LinearLayout>(R.id.content_main_ll)
        assertNotNull(view)
    }

    @Test
    fun testShowCalendar_onBeforeDateCardClicked() {
        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_before_cd)).perform(click())

        onView(withText("OK"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .check(matches(isDisplayed()))
    }

    @Test
    fun testShowCalendar_onAfterDateCardClicked() {
        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_after_cd)).perform(click())

        onView(withText("OK"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .check(matches(isDisplayed()))
    }

    @Test
    fun testDismissAfterCalendar() {

        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_after_cd)).perform(click())

        onView(withText("ANNULLA"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_after_tv)).check(matches(withText("")))
        onView(withId(R.id.content_main_brewed_after_close_iv)).check(matches(not(isDisplayed())))
    }

    @Test
    fun testDismissBeforeCalendar() {

        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_before_cd)).perform(click())

        onView(withText("ANNULLA"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_before_tv)).check(matches(withText("")))
        onView(withId(R.id.content_main_brewed_before_close_iv)).check(matches(not(isDisplayed())))
    }

    @Test
    fun testDismissAfterCalendar_withValue() {

        onView(withId(R.id.content_main_brewed_after_tv))
                .perform(setTextInTextView("05-2019"))

        onView(withId(R.id.content_main_brewed_after_cd)).perform(click())

        onView(withText("ANNULLA"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_after_tv)).check(matches(withText("05-2019")))
        onView(withId(R.id.content_main_brewed_after_close_iv)).check(matches(not(isDisplayed())))
    }

    @Test
    fun testDismissBeforeCalendar_withValue() {

        onView(withId(R.id.content_main_brewed_before_tv))
                .perform(setTextInTextView("05-2019"))

        onView(withId(R.id.content_main_brewed_before_cd)).perform(click())

        onView(withText("ANNULLA"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_before_tv)).check(matches(withText("05-2019")))
        onView(withId(R.id.content_main_brewed_before_close_iv)).check(matches(not(isDisplayed())))
    }

    @Test
    fun testPickAfterDate() {

        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_after_cd)).perform(click())

        onView(withText("OK"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_after_tv)).check(matches(not(withText(""))))
        onView(withId(R.id.content_main_brewed_after_close_iv)).check(matches(isDisplayed()))

    }

    @Test
    fun testPickBeforeDate() {
        // Preconditions
        // Check is all unset

        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_before_cd)).perform(click())

        onView(withText("OK"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_before_tv)).check(matches(not(withText(""))))
        onView(withId(R.id.content_main_brewed_before_close_iv)).check(matches(isDisplayed()))

    }

    @Test
    fun testBeforeClearIcon_withSelectedDate(){
        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_before_cd)).perform(click())

        onView(withText("OK"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_before_tv)).check(matches(not(withText(""))))
        onView(withId(R.id.content_main_brewed_before_close_iv)).check(matches(isDisplayed()))

        onView(withId(R.id.content_main_brewed_before_close_iv)).perform(click())

        onView(withId(R.id.content_main_brewed_before_tv)).check(matches(withText("")))
        onView(withId(R.id.content_main_brewed_before_close_iv)).check(matches(not(isDisplayed())))

    }

    @Test
    fun testAfterClearIcon_withSelectedDate(){
        this.checkEmptyFields()

        onView(withId(R.id.content_main_brewed_after_cd)).perform(click())

        onView(withText("OK"))
                .inRoot(withDecorView(not(`is`(activity?.window?.decorView))))
                .perform(click())

        onView(withId(R.id.content_main_brewed_after_tv)).check(matches(not(withText(""))))
        onView(withId(R.id.content_main_brewed_after_close_iv)).check(matches(isDisplayed()))

        onView(withId(R.id.content_main_brewed_after_close_iv)).perform(click())

        onView(withId(R.id.content_main_brewed_after_tv)).check(matches(withText("")))
        onView(withId(R.id.content_main_brewed_after_close_iv)).check(matches(not(isDisplayed())))

    }


    // Private class functions
    private fun checkEmptyFields() {
        onView(withId(R.id.content_main_name_edt)).check(matches(withText("")))
        onView(withId(R.id.content_main_brewed_after_tv)).check(matches(withText("")))
        onView(withId(R.id.content_main_brewed_before_tv)).check(matches(withText("")))

    }
}