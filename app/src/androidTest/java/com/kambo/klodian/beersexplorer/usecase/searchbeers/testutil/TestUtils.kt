package com.kambo.klodian.beersexplorer.usecase.searchbeers.testutil

import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.matcher.ViewMatchers
import android.view.View
import android.widget.TextView
import org.hamcrest.Matcher
import org.hamcrest.Matchers

class TestUtils {
    companion object{
        fun setTextInTextView(value: String): ViewAction {
            return object : ViewAction {
                override fun getConstraints(): Matcher<View> {
                    return Matchers.allOf(ViewMatchers.isDisplayed(), ViewMatchers.isAssignableFrom(TextView::class.java))
                }

                override fun perform(uiController: UiController, view: View) {
                    (view as TextView).text = value
                }

                override fun getDescription(): String {
                    return "replace text"
                }
            }
        }
    }
}