package com.kambo.klodian.beersexplorer.usecase.searchbeers

import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.kambo.klodian.beersexplorer.mockedCorrectBeerResponse
import com.kambo.klodian.beersexplorer.mockedMalformedBeerResponse
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.model.beer.BeerDeserializer
import org.junit.Assert
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class BeerDeserializerTest {

    private val correctDateFormat = SimpleDateFormat("MM/yyyy", Locale.getDefault())
    private val wrongDateFormat = SimpleDateFormat("mm-yyyy", Locale.getDefault())

    @Test
    fun testDeserialize_whenCorrectResponse() {

        val json =  JsonParser().parse(mockedCorrectBeerResponse)
        val jsonObject = json.asJsonArray

        val gson = GsonBuilder()
                .registerTypeAdapter(Beer::class.java, BeerDeserializer(correctDateFormat))
                .create()

        val results = jsonObject
                .mapNotNull { gson.fromJson(it, Beer::class.java) }
                .toMutableList()

        Assert.assertEquals(1,results.size)
    }

    @Test
    fun testDeserialize_whenMalformedResponse() {

        val json =  JsonParser().parse(mockedMalformedBeerResponse)
        val jsonObject = json.asJsonArray

        val gson = GsonBuilder()
                .registerTypeAdapter(Beer::class.java, BeerDeserializer(correctDateFormat))
                .create()

        val results = jsonObject
                .mapNotNull { gson.fromJson(it, Beer::class.java) }
                .toMutableList()

        Assert.assertEquals(0, results.size)
    }


    @Test
    fun testDeserialize_failWithWrongDateFormat() {

        val json =  JsonParser().parse(mockedCorrectBeerResponse)
        val jsonObject = json.asJsonArray

        val gson = GsonBuilder()
                .registerTypeAdapter(Beer::class.java, BeerDeserializer(wrongDateFormat))
                .create()

        val results = jsonObject.mapNotNull { gson.fromJson(it, Beer::class.java) }
                .toMutableList()

        Assert.assertEquals(null, results.first().firstBrewed)
    }
}