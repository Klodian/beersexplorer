package com.kambo.klodian.beersexplorer.usecase.searchbeers.mocks

import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.model.beer.IBeerModel
import java.util.*
import kotlin.collections.HashMap

class MockedBeerModel: IBeerModel {


    override val dateFormat: String
        get() = "MM-yyyy"

    override fun getBeers(searchTerm: HashMap<String, String>,
                          page: Int?,
                          completion: (itemList: MutableList<Beer>, error: Throwable?) -> Unit) {

        completion.invoke(itemList,null)
    }

    override fun buildSearchTerm(name: String?, brewedAfter: Date?, brewedBefore: Date?): HashMap<String, String> {
        return hashMapOf()
    }

    override val startingPage: Int?
        get() = 0
    override val maxPages: Int
        get() = 1
    private val itemList = arrayListOf(
            Beer(1,
                    "Mocked Name",
                    "Mocked Description",
                    "Mocked Tagline",
                    Date(),
                    "")
    )

    override fun getNextPage(currentPage: Int?): Int? { return 1 }

}