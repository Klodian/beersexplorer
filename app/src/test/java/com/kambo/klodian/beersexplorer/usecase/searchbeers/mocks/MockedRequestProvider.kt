package com.kambo.klodian.beersexplorer.usecase.searchbeers.mocks

import com.kambo.klodian.beersexplorer.network.*
import okhttp3.Request
import java.util.HashMap

class MockedRequestProvider : IRequestProvider {
    override fun getBeersRequest(searchTerm: HashMap<String, String>): Request {
        return OkHttpRequestBuilder().build(
                endpoint = "v2/beers",
                host = "api.punkapi.com",
                protocol = HttpProtocol.HTTPS,
                port = 443,
                method = RequestMethod.GET,
                queryParameters = searchTerm,
                body = null,
                headerParameters = hashMapOf()
        )
    }
}