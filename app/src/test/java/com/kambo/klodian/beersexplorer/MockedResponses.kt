package com.kambo.klodian.beersexplorer


val mockedMalformedBeerResponse = "[" +
        "{" +
        "\"Just\": \"a wrong Json\"" +
        "}" +
        "]"

val mockedCorrectBeerResponse =
        "[\n" +
                "  {\n" +
                "    \"id\": 192,\n" +
                "    \"name\": \"Punk IPA 2007 - 2010\",\n" +
                "    \"tagline\": \"Post Modern Classic. Spiky. Tropical. Hoppy.\",\n" +
                "    \"first_brewed\": \"04/2007\",\n" +
                "    \"description\": \"Our flagship beer that kick started the craft beer revolution. This is James and Martin's original take on an American IPA, subverted with punchy New Zealand hops. Layered with new world hops to create an all-out riot of grapefruit, pineapple and lychee before a spiky, mouth-puckering bitter finish.\",\n" +
                "    \"image_url\": \"https://images.punkapi.com/v2/192.png\",\n" +
                "    \"abv\": 6.0,\n" +
                "    \"ibu\": 60.0,\n" +
                "    \"target_fg\": 1010.0,\n" +
                "    \"target_og\": 1056.0,\n" +
                "    \"ebc\": 17.0,\n" +
                "    \"srm\": 8.5,\n" +
                "    \"ph\": 4.4,\n" +
                "    \"attenuation_level\": 82.14,\n" +
                "    \"volume\": {\n" +
                "      \"value\": 20,\n" +
                "      \"unit\": \"liters\"\n" +
                "    },\n" +
                "    \"boil_volume\": {\n" +
                "      \"value\": 25,\n" +
                "      \"unit\": \"liters\"\n" +
                "    },\n" +
                "    \"method\": {\n" +
                "      \"mash_temp\": [\n" +
                "        {\n" +
                "          \"temp\": {\n" +
                "            \"value\": 65,\n" +
                "            \"unit\": \"celsius\"\n" +
                "          },\n" +
                "          \"duration\": 75\n" +
                "        }\n" +
                "      ],\n" +
                "      \"fermentation\": {\n" +
                "        \"temp\": {\n" +
                "          \"value\": 19.0,\n" +
                "          \"unit\": \"celsius\"\n" +
                "        }\n" +
                "      },\n" +
                "      \"twist\": null\n" +
                "    },\n" +
                "    \"ingredients\": {\n" +
                "      \"malt\": [\n" +
                "        {\n" +
                "          \"name\": \"Extra Pale\",\n" +
                "          \"amount\": {\n" +
                "            \"value\": 5.3,\n" +
                "            \"unit\": \"kilograms\"\n" +
                "          }\n" +
                "        }\n" +
                "      ],\n" +
                "      \"hops\": [\n" +
                "        {\n" +
                "          \"name\": \"Ahtanum\",\n" +
                "          \"amount\": {\n" +
                "            \"value\": 17.5,\n" +
                "            \"unit\": \"grams\"\n" +
                "           },\n" +
                "           \"add\": \"start\",\n" +
                "           \"attribute\": \"bitter\"\n" +
                "         },\n" +
                "         {\n" +
                "           \"name\": \"Chinook\",\n" +
                "           \"amount\": {\n" +
                "             \"value\": 15,\n" +
                "             \"unit\": \"grams\"\n" +
                "           },\n" +
                "           \"add\": \"start\",\n" +
                "           \"attribute\": \"bitter\"\n" +
                "         },\n" +
                "         ...\n" +
                "      ],\n" +
                "      \"yeast\": \"Wyeast 1056 - American Ale™\"\n" +
                "    },\n" +
                "    \"food_pairing\": [\n" +
                "      \"Spicy carne asada with a pico de gallo sauce\",\n" +
                "      \"Shredded chicken tacos with a mango chilli lime salsa\",\n" +
                "      \"Cheesecake with a passion fruit swirl sauce\"\n" +
                "    ],\n" +
                "    \"brewers_tips\": \"While it may surprise you, this version of Punk IPA isn't dry hopped but still packs a punch! To make the best of the aroma hops make sure they are fully submerged and add them just before knock out for an intense hop hit.\",\n" +
                "    \"contributed_by\": \"Sam Mason <samjbmason>\"\n" +
                "  }\n" +
                "]"

