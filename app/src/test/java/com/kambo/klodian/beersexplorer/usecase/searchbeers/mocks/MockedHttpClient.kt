package com.kambo.klodian.beersexplorer.usecase.searchbeers.mocks

import com.kambo.klodian.beersexplorer.network.IOkHttpClient
import okhttp3.*
import java.io.IOException

class MockedHttpClient(var shouldFailRequest: Boolean = false,
                       var expectedJsonBody: String) : IOkHttpClient {

    private val okHttpClient = OkHttpClient.Builder().build()

    override fun send(request: Request, callback: Callback) {
        val call = okHttpClient.newCall(request)
        if (shouldFailRequest) {
            callback.onFailure(call, IOException("onFailure"))
        } else {
            val responseBody = ResponseBody.create(MediaType.parse("application-json"), expectedJsonBody)
            callback.onResponse(call,
                    Response.Builder()
                            .request(request)
                            .protocol(Protocol.HTTP_2)
                            .code(200)
                            .message("OK")
                            .body(responseBody)
                            .build())
        }
    }

}