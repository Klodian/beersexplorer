package com.kambo.klodian.beersexplorer

import android.arch.lifecycle.Observer

class LoggingObserver<T> : Observer<T> {
    var value : T? = null
    override fun onChanged(t: T?) {
        this.value = t
    }
}