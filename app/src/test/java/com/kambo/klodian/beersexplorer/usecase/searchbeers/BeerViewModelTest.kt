package com.kambo.klodian.beersexplorer.usecase.searchbeers

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.paging.PagedList
import com.kambo.klodian.beersexplorer.LoggingObserver
import com.kambo.klodian.beersexplorer.extension.set
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.usecase.searchbeers.mocks.MockedBeerModel
import junit.framework.Assert
import org.junit.Rule
import org.junit.Test
import java.util.*

class BeerViewModelTest {

    // used to make all live data calls sync
    @get:Rule
    val instantExecutor = InstantTaskExecutorRule()

    private val mockedPagingConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setPrefetchDistance(25)
            .setPageSize(25)
            .build()

    // view Model with mocked MockedBeerModel returning data immediately
    private val beerViewModel = BeerViewModel(MockedBeerModel(), mockedPagingConfig)

    private val observer = LoggingObserver<PagedList<Beer>>()

    @Test
    fun testGetItemListNotEmpty_whenConsistentSearchParam() {
        beerViewModel
                .getBeersLiveData(null, null, null)
                .observeForever(observer)

        Assert.assertEquals(1, observer.value?.size)
    }


    @Test
    fun testSetGetAfterStoredDate() {

        val dateToStore = Date()

        Assert.assertEquals(null, beerViewModel.getAfterDate())

        beerViewModel.storeAfterDate(dateToStore)

        Assert.assertEquals(dateToStore, beerViewModel.getAfterDate())

    }

    @Test
    fun testSetGetBeforeStoredDate() {

        val dateToStore = Date()

        Assert.assertEquals(null, beerViewModel.getBeforeDate())

        beerViewModel.storeBeforeDate(dateToStore)

        Assert.assertEquals(dateToStore, beerViewModel.getBeforeDate())

    }

    @Test
    fun testResetAfterStoredDate() {

        val dateToStore = Date()
        beerViewModel.storeAfterDate(dateToStore)
        Assert.assertEquals(dateToStore, beerViewModel.getAfterDate())

        beerViewModel.storeAfterDate(null)
        Assert.assertEquals(null, beerViewModel.getAfterDate())


    }

    @Test
    fun testResetBeforeStoredDate() {

        val dateToStore = Date()
        beerViewModel.storeBeforeDate(dateToStore)
        Assert.assertEquals(dateToStore, beerViewModel.getBeforeDate())

        beerViewModel.storeBeforeDate(null)
        Assert.assertEquals(null, beerViewModel.getBeforeDate())

    }

}