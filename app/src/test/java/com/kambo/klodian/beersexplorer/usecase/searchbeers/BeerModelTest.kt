package com.kambo.klodian.beersexplorer.usecase.searchbeers

import com.kambo.klodian.beersexplorer.extension.set
import com.kambo.klodian.beersexplorer.mockedCorrectBeerResponse
import com.kambo.klodian.beersexplorer.mockedMalformedBeerResponse
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.model.beer.BeerModel
import com.kambo.klodian.beersexplorer.usecase.searchbeers.mocks.MockedHttpClient
import com.kambo.klodian.beersexplorer.usecase.searchbeers.mocks.MockedRequestProvider
import org.junit.Assert
import org.junit.Test
import java.util.*

class BeerModelTest {

    private val mockedRequestProvider = MockedRequestProvider()

    @Test
    fun testBuildSearchTerm() {

        val testInputName = "Buzz"

        val expectedBeerName = "Buzz"

        val testInputBrewedAfterDate = Date()
        testInputBrewedAfterDate.set(2005,5,null)
        val expectedAfterDate = "05-2005"

        val testInputBrewedBeforeDate = Date()
        testInputBrewedBeforeDate.set(2019,10,null)
        val expectedBeforeDate = "10-2019"

        val expectedSearchTerm = hashMapOf(
                Pair("brewed_after", expectedAfterDate),
                Pair("brewed_before", expectedBeforeDate),
                Pair("beer_name", expectedBeerName)
        )

        val beerModel = BeerModel(MockedHttpClient(false, mockedCorrectBeerResponse), mockedRequestProvider)

        val restResultSearchTerm = beerModel.buildSearchTerm(testInputName,testInputBrewedAfterDate,testInputBrewedBeforeDate)

        Assert.assertEquals(expectedSearchTerm,restResultSearchTerm)
    }

    @Test
    fun testGetBeersNonZeroResults_WithCorrectResponse() {
        val beerModel = BeerModel(MockedHttpClient(false, mockedCorrectBeerResponse), mockedRequestProvider)

        beerModel.getBeers(hashMapOf(),1) { mutableList: MutableList<Beer>, throwable: Throwable? ->
            Assert.assertEquals(1,mutableList.size)
            Assert.assertEquals(null, throwable)
        }
    }

    @Test
    fun testGetBeersZeroResults_WithMalformedResponse() {
        val beerModel = BeerModel(MockedHttpClient(false, mockedMalformedBeerResponse), mockedRequestProvider)

        beerModel.getBeers(hashMapOf(),1) { mutableList: MutableList<Beer>, throwable: Throwable? ->
            Assert.assertEquals(0,mutableList.size)
            Assert.assertEquals(null, throwable)
        }
    }


    @Test
    fun testGetBeersFailure_WithFailedCallback() {
        val beerModel = BeerModel(MockedHttpClient(true, mockedCorrectBeerResponse), mockedRequestProvider)

        beerModel.getBeers(hashMapOf(),1) { mutableList: MutableList<Beer>, throwable: Throwable? ->
            Assert.assertEquals(0,mutableList.size)
            Assert.assertNotNull(throwable)
        }
    }
}