package com.kambo.klodian.beersexplorer.usecase.searchbeers

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Toast
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment
import com.kambo.klodian.beersexplorer.BeersExplorerApplication
import com.kambo.klodian.beersexplorer.R
import com.kambo.klodian.beersexplorer.androidutil.hideKeyboard
import com.kambo.klodian.beersexplorer.databinding.SearchBeersLayoutBinding
import com.kambo.klodian.beersexplorer.extension.getActualYear
import com.kambo.klodian.beersexplorer.extension.getMonthOfYear
import com.kambo.klodian.beersexplorer.extension.set
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.network.NetworkState
import com.kambo.klodian.beersexplorer.network.Status
import com.kambo.klodian.beersexplorer.usecase.searchbeers.adapter.BeerItemAdapter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class BeerActivity : AppCompatActivity() {

    companion object {
        private const val TAG_DATE_PICKER_DIALOG_BEFORE = "TAG_DATE_PICKER_DIALOG_BEFORE"
        private const val TAG_DATE_PICKER_DIALOG_AFTER = "TAG_DATE_PICKER_DIALOG_AFTER"
    }

    private lateinit var adapter: BeerItemAdapter

    private lateinit var binding: SearchBeersLayoutBinding
    private lateinit var viewModel: IBeerViewModel
    private lateinit var dateFormatter: SimpleDateFormat

    @Inject
    lateinit var beerViewModelFactory: BeerViewModelFactory

    private var beforeDatePickerFragment: MonthYearPickerDialogFragment? = null
    private var afterDatePickerFragment: MonthYearPickerDialogFragment? = null

    private val beerResultsObserver = Observer<PagedList<Beer>> { item ->
        item?.let { itemList ->
            adapter.submitList(itemList)
        }
    }

    private val networkStateObserver = Observer<NetworkState> { networkState ->
        networkState?.let {
            when (it.status) {
                Status.FAILED -> {
                    setProgressBarEnabled(false)
                    Toast.makeText(this, "Request Failed", Toast.LENGTH_SHORT).show()
                    adapter.setLoadingEnabled(false)
                }
                Status.RUNNING -> {
                    setProgressBarEnabled(true)
                    adapter.setLoadingEnabled(true)
                }
                Status.SUCCESS -> {
                    setProgressBarEnabled(false)
                    adapter.setLoadingEnabled(false)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        (application as BeersExplorerApplication)
                .component
                .inject(this)

        viewModel = ViewModelProviders
                .of(this, beerViewModelFactory)
                .get(BeerViewModel::class.java)

        dateFormatter = SimpleDateFormat(viewModel.getDateFormat(), Locale.getDefault())

        binding = DataBindingUtil.setContentView(this, R.layout.search_beers_layout)

        setProgressBarEnabled(false)

        viewModel.getLastBeersLiveData().observe(this@BeerActivity, beerResultsObserver)

        binding.contentMainSearchFab.setOnClickListener {
            val dateBeforeString = binding.contentMainBrewedBeforeTv.text.toString()
            val dateAfterString = binding.contentMainBrewedAfterTv.text.toString()
            val beforeDate = if (dateBeforeString.isBlank()) null else dateFormatter.parse(dateBeforeString)
            val afterDate = if (dateAfterString.isBlank()) null else dateFormatter.parse(dateAfterString)
            val name = binding.contentMainNameEdt.text.toString()
            val nameToSearch = if (name.isBlank()) null else name

            hideKeyboard(this)
            setProgressBarEnabled(true)

            // Valid searchParam, perform search
            viewModel.getBeersLiveData(nameToSearch, afterDate, beforeDate)
                    .observe(this@BeerActivity, beerResultsObserver)

            // observer for network state
            viewModel.getNetworkState().observe(this, networkStateObserver)
        }

        binding.contentMainRv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        adapter = BeerItemAdapter(dateFormatter) { onPosition ->
            adapter.currentList!![onPosition]?.let { beer ->

            }
        }

        binding.contentMainRv.adapter = adapter

        binding.contentMainBrewedBeforeCd.setOnClickListener {

            val currentDate = this.viewModel.getBeforeDate() ?: Date()

            beforeDatePickerFragment = MonthYearPickerDialogFragment.getInstance(
                    currentDate.getMonthOfYear(),
                    currentDate.getActualYear())

            beforeDatePickerFragment?.setOnDateSetListener { year, month ->
                val date = Date()
                // Library returns values from 0 to 11, but human readable set api is meant to be from 1 to 12
                date.set(year, month + 1, null)

                this.viewModel.storeBeforeDate(date)

                binding.contentMainBrewedBeforeTv.text = dateFormatter.format(date)
                this@BeerActivity.setClearBeforeDateIvEnabled(true)
            }

            beforeDatePickerFragment?.show(supportFragmentManager, TAG_DATE_PICKER_DIALOG_BEFORE)
        }

        binding.contentMainBrewedAfterCd.setOnClickListener {

            val currentDate = this.viewModel.getAfterDate() ?: Date()
            afterDatePickerFragment = MonthYearPickerDialogFragment.getInstance(currentDate.getMonthOfYear(), currentDate.getActualYear())

            afterDatePickerFragment?.setOnDateSetListener { year, month ->
                val date = Date()
                // Library returns values from 0 to 11, but human readable set api is meant to be from 1 to 12
                date.set(year, month + 1, null)
                this.viewModel.storeAfterDate(date)

                binding.contentMainBrewedAfterTv.text = dateFormatter.format(date)
                this@BeerActivity.setClearAfterDateIvEnabled(true)
            }

            afterDatePickerFragment?.show(supportFragmentManager, TAG_DATE_PICKER_DIALOG_AFTER)
        }

        binding.contentMainBrewedAfterCloseIv.setOnClickListener {
            binding.contentMainBrewedAfterTv.text = ""
            this@BeerActivity.setClearAfterDateIvEnabled(false)
            this.viewModel.storeAfterDate(null)
        }

        binding.contentMainBrewedBeforeCloseIv.setOnClickListener {
            binding.contentMainBrewedBeforeTv.text = ""
            this@BeerActivity.setClearBeforeDateIvEnabled(false)
            this.viewModel.storeBeforeDate(null)
        }

        if (savedInstanceState != null) {

            this.viewModel.getBeforeDate()?.let {
                this.setBeforeDateCardLayout(it)
            }

            this.viewModel.getAfterDate()?.let {
                this.setAfterDateCardLayout(it)
            }

            // MonthYearPickerDialogFragment fails to handle the rotation on DateListener
        }
    }

    // private class functions *********************************************************************
    private fun setProgressBarEnabled(isEnabled: Boolean) {
        binding.contentMainProgressBar.visibility =
                if (isEnabled) View.VISIBLE else View.GONE
    }

    private fun setClearBeforeDateIvEnabled(isEnabled: Boolean) {
        binding.contentMainBrewedBeforeCloseIv.visibility =
                if (isEnabled) View.VISIBLE else View.INVISIBLE
    }

    private fun setClearAfterDateIvEnabled(isEnabled: Boolean) {
        binding.contentMainBrewedAfterCloseIv.visibility =
                if (isEnabled) View.VISIBLE else View.INVISIBLE
    }

    private fun setAfterDateCardLayout(date: Date) {
        binding.contentMainBrewedAfterTv.text = dateFormatter.format(date)
        this.setClearAfterDateIvEnabled(true)

    }

    private fun setBeforeDateCardLayout(date: Date) {
        binding.contentMainBrewedBeforeTv.text = dateFormatter.format(date)
        this.setClearBeforeDateIvEnabled(true)
    }

}
