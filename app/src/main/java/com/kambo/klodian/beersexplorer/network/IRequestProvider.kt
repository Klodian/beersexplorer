package com.kambo.klodian.beersexplorer.network

import okhttp3.Request

interface IRequestProvider {

    fun getBeersRequest(searchTerm: HashMap<String, String>): Request
}