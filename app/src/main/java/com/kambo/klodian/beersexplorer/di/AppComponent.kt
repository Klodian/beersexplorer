package com.kambo.klodian.beersexplorer.di

import com.kambo.klodian.beersexplorer.usecase.searchbeers.BeerActivity
import dagger.Component

@ApplicationScope
@Component(modules = [NetworkModule::class, ContextModule::class, ModelModule::class])

interface AppComponent {

    fun inject(beerActivity: BeerActivity)
}