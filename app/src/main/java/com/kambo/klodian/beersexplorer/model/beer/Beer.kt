package com.kambo.klodian.beersexplorer.model.beer

import android.support.v7.util.DiffUtil
import java.util.*


data class Beer(
        var id: Long,
        var name: String,
        var description: String?,
        var tagline: String,
        var firstBrewed: Date?,
        var imageUrl: String?) {

    companion object {
        var diffCallback: DiffUtil.ItemCallback<Beer> = object : DiffUtil.ItemCallback<Beer>() {
            override fun areItemsTheSame(oldItem: Beer, newItem: Beer): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Beer, newItem: Beer): Boolean {
                return oldItem == newItem
            }
        }
    }
}