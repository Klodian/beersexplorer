package com.kambo.klodian.beersexplorer.usecase.searchbeers.adapter

import android.support.v7.widget.RecyclerView
import com.kambo.klodian.beersexplorer.databinding.BeerRowBinding
import com.squareup.picasso.Picasso

class BeerVh(private val binding: BeerRowBinding): RecyclerView.ViewHolder(binding.root){

    class Layout(val beerName: String,
                 val description: String?,
                 val firstBrewed: String,
                 val tagline: String,
                 var beerImgUrl: String?)

    fun bind(layout: Layout, clickAction :((forPosition: Int) -> Unit)){

        binding.layout = layout

        if(!layout.beerImgUrl.isNullOrBlank()) {
            Picasso.with(binding.root.context).load(layout.beerImgUrl)
                    .resize(160,280)
                    .centerInside()
                    .into(binding.beerRowIconIv)
        }

        binding.root.setOnClickListener {
            clickAction.invoke(adapterPosition)
        }
    }
}