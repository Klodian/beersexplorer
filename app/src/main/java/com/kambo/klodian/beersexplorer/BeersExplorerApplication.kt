package com.kambo.klodian.beersexplorer

import android.app.Application
import com.kambo.klodian.beersexplorer.di.AppComponent
import com.kambo.klodian.beersexplorer.di.ContextModule
import com.kambo.klodian.beersexplorer.di.DaggerAppComponent
import timber.log.Timber

class BeersExplorerApplication : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .contextModule(ContextModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}