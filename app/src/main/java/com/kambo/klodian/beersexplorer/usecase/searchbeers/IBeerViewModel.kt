package com.kambo.klodian.beersexplorer.usecase.searchbeers

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.network.NetworkState
import java.util.*

interface IBeerViewModel{

    fun getNetworkState(): LiveData<NetworkState>
    fun getBeersLiveData(name: String?,
                         brewedAfter: Date?,
                         brewedBefore: Date?) : LiveData<PagedList<Beer>>

    fun getDateFormat(): String
    fun getLastBeersLiveData(): LiveData<PagedList<Beer>>

    fun getAfterDate(): Date?
    fun getBeforeDate(): Date?
    fun storeBeforeDate(date: Date?)
    fun storeAfterDate(date: Date?)
}