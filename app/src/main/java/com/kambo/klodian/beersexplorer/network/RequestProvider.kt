package com.kambo.klodian.beersexplorer.network

import okhttp3.Request

class RequestProvider: IRequestProvider{

    companion object {

        private const val host = "api.punkapi.com"
        private const val port = 443

        private fun getBeersEndPoint() = "v2/beers"
    }

    override fun getBeersRequest(searchTerm: HashMap<String,String>): Request {

        return OkHttpRequestBuilder().build(
                endpoint = getBeersEndPoint(),
                host = host,
                protocol = HttpProtocol.HTTPS,
                port = port,
                method = RequestMethod.GET,
                queryParameters = searchTerm,
                body = null,
                headerParameters = hashMapOf()
        )
    }
}