package com.kambo.klodian.beersexplorer.usecase.searchbeers

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.paging.PagedList
import com.kambo.klodian.beersexplorer.model.beer.IBeerModel
import javax.inject.Inject

class BeerViewModelFactory @Inject constructor(private val beerModel: IBeerModel,
                                               private val pagedListConfig: PagedList.Config) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BeerViewModel(beerModel, pagedListConfig) as T
    }
}