package com.kambo.klodian.beersexplorer.di

import javax.inject.Scope

@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class ApplicationScope
