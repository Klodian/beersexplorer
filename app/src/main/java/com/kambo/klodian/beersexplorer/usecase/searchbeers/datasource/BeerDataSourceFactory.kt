package com.kambo.klodian.beersexplorer.usecase.searchbeers.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.kambo.klodian.beersexplorer.model.beer.IBeerModel
import com.kambo.klodian.beersexplorer.model.beer.Beer

class BeerDataSourceFactory(private val beerModel: IBeerModel,
                            private val searchTerm: HashMap<String, String>?)
    : DataSource.Factory<Int, Beer>() {

    val beersDataSourceLiveData = MutableLiveData<BeerDataSource>()

    override fun create(): DataSource<Int, Beer> {
        val beerDataSource = BeerDataSource(beerModel, searchTerm)
        beersDataSourceLiveData.postValue(beerDataSource)
        return beerDataSource
    }
}