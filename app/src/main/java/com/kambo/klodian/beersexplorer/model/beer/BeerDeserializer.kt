package com.kambo.klodian.beersexplorer.model.beer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.kambo.klodian.beersexplorer.extension.parseLong
import com.kambo.klodian.beersexplorer.extension.parseString
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BeerDeserializer(private val dateFormat: SimpleDateFormat) : JsonDeserializer<Beer> {

    companion object {
        private const val ID = "id"
        private const val NAME = "name"
        private const val IMAGE_URL = "image_url"
        private const val DESCRIPTION = "description"
        private const val TAGLINE = "tagline"
        private const val FIRST_BREWED = "first_brewed"
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext): Beer? {
        json?.asJsonObject?.let { jsonObject ->

            val id = jsonObject.parseLong(ID) ?: return null
            val name = jsonObject.parseString(NAME) ?: return null

            val htmlUrl = jsonObject.parseString(IMAGE_URL)

            val description = jsonObject.parseString(DESCRIPTION)

            val tagline = jsonObject.parseString(TAGLINE) ?: "--"

            val firstBrewed = jsonObject.parseString(FIRST_BREWED)

            var firstBrewedDate : Date? = null
            try{
                firstBrewedDate = firstBrewed?.let { dateFormat.parse(it)}
            } catch (parseError: ParseException){
                // Wrong format
            }

            return Beer(
                    id = id,
                    name = name,
                    imageUrl = htmlUrl,
                    description = description,
                    firstBrewed = firstBrewedDate,
                    tagline = tagline)
        }
        return null
    }

}