package com.kambo.klodian.beersexplorer.di

import android.arch.paging.PagedList
import com.kambo.klodian.beersexplorer.configurations.AppConfiguration
import com.kambo.klodian.beersexplorer.configurations.IConfiguration
import dagger.Module
import dagger.Provides

@Module
class ConfigurationModule {

    @Provides
    @ApplicationScope
    fun provideConfiguration(): IConfiguration {
        return AppConfiguration()
    }

    @Provides
    @ApplicationScope
    fun providePaginationConfig(): PagedList.Config {
        return PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setPrefetchDistance(25)
                .setPageSize(25)
                .build()
    }

}