package com.kambo.klodian.beersexplorer.model.beer

import java.util.*
import kotlin.collections.HashMap

interface IBeerModel {
    val dateFormat: String
    val startingPage: Int?

    val maxPages: Int

    fun getBeers(searchTerm: HashMap<String,String>,
                 page: Int?,
                 completion: (itemList: MutableList<Beer>, error: Throwable?) -> Unit)

    fun getNextPage(currentPage: Int?): Int?
    fun buildSearchTerm(name: String?, brewedAfter: Date?, brewedBefore: Date?): HashMap<String, String>
}