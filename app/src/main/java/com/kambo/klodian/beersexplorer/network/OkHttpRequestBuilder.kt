package com.kambo.klodian.beersexplorer.network

import okhttp3.MediaType
import okhttp3.RequestBody


enum class RequestMethod {
    GET, POST, PUT, DELETE, PATCH, HEAD
}

enum class HttpProtocol(val protocol: String) {
    HTTP("http"),
    HTTPS("https")
}

class OkHttpRequestBuilder {

    fun build(protocol:         HttpProtocol,
              host:             String,
              endpoint:         String,
              port:             Int,
              method:           RequestMethod,
              queryParameters:  MutableMap<String, String>?,
              headerParameters: MutableMap<String, String>?,
              body:             Any?): okhttp3.Request {

        // Url *************************************************************************************

        val urlBuilder = okhttp3.HttpUrl.Builder()
        urlBuilder.scheme(protocol.protocol)
        urlBuilder.host(host)
        urlBuilder.port(port)
        urlBuilder.addPathSegments(endpoint)

        queryParameters?.let {
            queryParameters.forEach {
                urlBuilder.addEncodedQueryParameter(it.key, it.value)
            }
        }

        // Request *********************************************************************************

        val requestBuilder = okhttp3.Request.Builder().url(urlBuilder.build())

        headerParameters?.let {
            headerParameters.forEach {
                requestBuilder.addHeader(it.key, it.value)
            }
        }

        when(method) {
            RequestMethod.GET -> {
                requestBuilder.get()
            }

            RequestMethod.HEAD -> {
                requestBuilder.head()
            }

            else -> {

                when(body) {
                    // Json Body
                    is String -> {
                        val mediaType = MediaType.parse("application/json; charset=utf-8")
                        requestBuilder.method(method.name, RequestBody.create(mediaType, body))
                    }
                }
            }
        }

        return requestBuilder.build()
    }
}