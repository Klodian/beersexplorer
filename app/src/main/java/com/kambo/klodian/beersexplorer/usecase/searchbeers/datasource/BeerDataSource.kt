package com.kambo.klodian.beersexplorer.usecase.searchbeers.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.model.beer.IBeerModel
import com.kambo.klodian.beersexplorer.network.NetworkState


class BeerDataSource(private val beerModel: IBeerModel,
                     private var searchTerm: HashMap<String, String>?) : PageKeyedDataSource<Int, Beer>() {


    var networkState = MutableLiveData<NetworkState>()


    override fun loadInitial(params: LoadInitialParams<Int>,
                             callback: LoadInitialCallback<Int, Beer>) {

        searchTerm?.let {
            networkState.postValue(NetworkState.LOADING)

            // Initial page
            val page = beerModel.startingPage
            beerModel.getBeers(it, page) { itemList, error ->

                if (error != null) {
                    networkState.postValue(NetworkState.error(error.message))
                } else {
                    networkState.postValue(NetworkState.LOADED)
                    callback.onResult(itemList, 0, page)
                }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>,
                            callback: LoadCallback<Int, Beer>) {
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Beer>) {

        searchTerm?.let {

            val page = params.key
            if (page < beerModel.maxPages) {
                networkState.postValue(NetworkState.LOADING)
                // Next page.
                val nextPage = beerModel.getNextPage(page)
                beerModel.getBeers(it, nextPage) { itemList, error ->

                    if (error != null) {
                        networkState.postValue(NetworkState.error(error.message))
                    } else {
                        networkState.postValue(NetworkState.LOADED)
                        callback.onResult(itemList, nextPage)
                    }
                }
            }
        }
    }
}