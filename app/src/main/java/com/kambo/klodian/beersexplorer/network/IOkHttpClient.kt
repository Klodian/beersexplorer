package com.kambo.klodian.beersexplorer.network

import okhttp3.Callback
import okhttp3.Request

interface IOkHttpClient {
    fun send(request: Request, callback: Callback)
}