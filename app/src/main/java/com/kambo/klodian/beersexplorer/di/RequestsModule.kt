package com.kambo.klodian.beersexplorer.di

import com.kambo.klodian.beersexplorer.network.IRequestProvider
import com.kambo.klodian.beersexplorer.network.RequestProvider
import dagger.Module
import dagger.Provides


@Module
class RequestsModule {

    @Provides
    @ApplicationScope
    fun getRequestProvider(): IRequestProvider {
        return RequestProvider()
    }

}