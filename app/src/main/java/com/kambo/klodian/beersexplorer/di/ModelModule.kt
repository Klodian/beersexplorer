package com.kambo.klodian.beersexplorer.di

import com.kambo.klodian.beersexplorer.model.beer.BeerModel
import com.kambo.klodian.beersexplorer.model.beer.IBeerModel
import com.kambo.klodian.beersexplorer.network.IOkHttpClient
import com.kambo.klodian.beersexplorer.network.IRequestProvider
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class, RequestsModule::class])
class ModelModule {

    @Provides
    @ApplicationScope
    fun getBeerModel(okHttpClient: IOkHttpClient, requestProvider: IRequestProvider): IBeerModel {
        return BeerModel(okHttpClient, requestProvider)
    }

}