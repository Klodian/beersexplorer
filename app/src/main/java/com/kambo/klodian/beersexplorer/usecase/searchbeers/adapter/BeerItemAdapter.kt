package com.kambo.klodian.beersexplorer.usecase.searchbeers.adapter

import android.arch.paging.PagedListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.kambo.klodian.beersexplorer.R
import com.kambo.klodian.beersexplorer.databinding.BeerRowBinding
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.network.NetworkState
import java.text.SimpleDateFormat


class BeerItemAdapter(private val simpleDateFormatter: SimpleDateFormat,
                      private val clickAction: (forPosition: Int) -> Unit) : PagedListAdapter<Beer, RecyclerView.ViewHolder>(Beer.diffCallback) {

    private var hasLoadingItem: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            R.layout.beer_row -> {
                val binding = BeerRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                BeerVh(binding)
            }
            R.layout.item_loading -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
                LoadingVh(view)
            }
            else -> throw IllegalArgumentException("unknown view type")
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.beer_row -> (holder as? BeerVh)?.let { vh ->
                getItem(vh.adapterPosition)?.let { item ->
                    vh.bind(map(item), clickAction)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasLoadingItem && position == itemCount - 1) {
            R.layout.item_loading
        } else {
            R.layout.beer_row
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasLoadingItem) 1 else 0
    }

    fun setLoadingEnabled(isEnabled: Boolean){
        hasLoadingItem = isEnabled
        if(isEnabled) {
            notifyItemInserted(itemCount)
        } else {
            notifyItemRemoved(itemCount)
        }
    }

    private fun map(beer: Beer): BeerVh.Layout {

        val firstBrewed =
                beer.firstBrewed?.let { simpleDateFormatter.format(it) } ?: " -- "

        return BeerVh.Layout(beer.name,
                beer.description,
                firstBrewed,
                beer.tagline,
                beer.imageUrl)
    }
}