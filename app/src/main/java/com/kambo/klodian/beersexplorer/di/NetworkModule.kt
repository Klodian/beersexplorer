package com.kambo.klodian.beersexplorer.di

import android.content.Context
import com.kambo.klodian.beersexplorer.configurations.IConfiguration
import com.kambo.klodian.beersexplorer.network.IOkHttpClient
import com.kambo.klodian.beersexplorer.network.OkHttpWrapper
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit


@Module(includes = [(ConfigurationModule::class)])
class NetworkModule {

    @Provides
    @ApplicationScope
    fun provideOkHttpClient(cache: Cache, loggingInterceptor: HttpLoggingInterceptor, configuration: IConfiguration): IOkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .cache(cache)
                .retryOnConnectionFailure(true)
                .connectTimeout(configuration.requestTimeout, TimeUnit.SECONDS)
                .readTimeout(configuration.requestTimeout, TimeUnit.SECONDS)
                .writeTimeout(configuration.requestTimeout, TimeUnit.SECONDS)
                .build()

        return OkHttpWrapper(okHttpClient)
    }


    @Provides
    @ApplicationScope
    fun provideCache(cacheFile: File): Cache {
        val cacheSize = 10 * 1024 * 1024L // 10 MB
        return Cache(cacheFile, cacheSize)
    }

    @Provides
    @ApplicationScope
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor(HttpLoggingInterceptor
                .Logger { message -> Timber.i(message) })
                .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @ApplicationScope
    fun provideCacheFile(context: Context): File {
        return File(context.cacheDir, "okhttp_cache")
    }

}

