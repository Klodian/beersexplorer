package com.kambo.klodian.beersexplorer.configurations

interface IConfiguration {
    val environmentVariables: IEnvironmentVariables
    
    // Networking **********************************************************************************
    val requestTimeout
        get() = environmentVariables.requestTimeout

}