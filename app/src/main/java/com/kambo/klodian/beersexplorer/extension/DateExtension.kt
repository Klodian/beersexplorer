package com.kambo.klodian.beersexplorer.extension

import java.util.*

fun Date.getActualYear(): Int {
    val calendar = Calendar.getInstance()
    calendar.time = this

    return calendar.get(Calendar.YEAR)
}

fun Date.getDayOfMonth(): Int {
    val calendar = Calendar.getInstance()
    calendar.time = this

    return calendar.get(Calendar.DAY_OF_MONTH)
}

fun Date.getMonthOfYear(): Int {
    val calendar = Calendar.getInstance()
    calendar.time = this

    return calendar.get(Calendar.MONTH)
}

fun Date.set(year: Int?, month: Int?, day: Int?) {
    val calendar = Calendar.getInstance()
    calendar.time = this

    year?.let { calendar.set(Calendar.YEAR, it) }

    // month goes from 0 to 12
    month?.let { calendar.set(Calendar.MONTH, it - if(it > 0) 1 else 0) }

    day?.let { calendar.set(Calendar.DAY_OF_MONTH, it) }

    this.time = calendar.time.time
}