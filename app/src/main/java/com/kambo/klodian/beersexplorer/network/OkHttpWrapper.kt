package com.kambo.klodian.beersexplorer.network

import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request

// Just a wrapper for testing purpose
class OkHttpWrapper(val okHttpClient: OkHttpClient): IOkHttpClient {
    override fun send(request: Request, callback: Callback) {
        okHttpClient.newCall(request).enqueue(callback)
    }
}