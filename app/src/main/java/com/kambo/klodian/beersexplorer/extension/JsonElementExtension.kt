package com.kambo.klodian.beersexplorer.extension

import com.google.gson.JsonObject

fun JsonObject.parseString(key:String): String?{
    if (this.isJsonNull) return null

    val jsonField = this[key]
    return if (jsonField == null || jsonField.isJsonNull) null else jsonField.asString
}

fun JsonObject.parseLong(key:String): Long?{
    if (this.isJsonNull) return null

    val jsonField = this[key]
    return if (jsonField == null || jsonField.isJsonNull) null else jsonField.asLong
}