package com.kambo.klodian.beersexplorer.model.beer

import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException
import com.kambo.klodian.beersexplorer.network.IOkHttpClient
import com.kambo.klodian.beersexplorer.network.IRequestProvider
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class BeerModel(private val okHttpClient: IOkHttpClient,
                private val requestProvider: IRequestProvider) : IBeerModel {


    // https://punkapi.com/documentation/v2 has a format error:
    // "brewed_before	date	Returns all beers brewed before this date, the date format is mm-yyyy e.g 10-2011"
    // mm-yyyy is intended to be MM-yyyy since mm is the format of the minutes, not of months
    override val dateFormat = "MM-yyyy"
    override var maxPages = 1
    override val startingPage = 1


    companion object {
        private const val dateResponseFormat = "MM/yyyy"

        private const val QUERY_PARAM_BEER_NAME = "beer_name"
        private const val QUERY_PARAM_BREWED_BEFORE = "brewed_before"
        private const val QUERY_PARAM_BREWED_AFTER = "brewed_after"
        private const val QUERY_PARAM_PAGE = "page"
    }


    private val dateFormatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    private val dateResponseFormatter = SimpleDateFormat(dateResponseFormat, Locale.getDefault())

    private var currentPage: Int = 1

    override fun getNextPage(currentPage: Int?): Int? {
        if (currentPage == maxPages) return null

        this.currentPage = currentPage?.let { page ->
            page + 1
        } ?: startingPage
        return this.currentPage
    }

    override fun getBeers(searchTerm: HashMap<String, String>,
                          page: Int?,
                          completion: ((itemList: MutableList<Beer>, error: Throwable?) -> Unit)) {

        searchTerm[QUERY_PARAM_PAGE] = page?.toString() ?: startingPage.toString()

        val request = requestProvider.getBeersRequest(searchTerm)
        okHttpClient.send(request, object : Callback {

            override fun onFailure(call: Call?, e: IOException?) {
                completion.invoke(arrayListOf(), e)
            }

            override fun onResponse(call: Call?, response: Response?) {
                response?.let { unwrappedResponse ->
                    if (unwrappedResponse.isSuccessful) {
                        // Parse Body
                        unwrappedResponse.body()?.let { responseBody ->

                            val parser = JsonParser()
                            try {
                                val jsonTree = parser.parse(responseBody.string())

                                if (!jsonTree.isJsonNull && jsonTree.isJsonArray) {

                                    val gson = GsonBuilder()
                                            .registerTypeAdapter(Beer::class.java, BeerDeserializer(dateResponseFormatter))
                                            .create()

                                    if (jsonTree.asJsonArray.size() > 0) {
                                        maxPages += 1
                                    }

                                    val itemList = jsonTree.asJsonArray
                                            .mapNotNull { gson.fromJson(it, Beer::class.java) }
                                            .toMutableList()

                                    if (itemList.size > 0) {
                                        currentPage += 1
                                    }

                                    completion.invoke(itemList, null)

                                } else {
                                    completion.invoke(arrayListOf(), Throwable("Not a JsonArray or Null"))
                                }
                            } catch (error: JsonSyntaxException) {
                                completion.invoke(arrayListOf(), error)
                            } catch (error: IOException) {
                                completion.invoke(arrayListOf(), error)
                            }
                        } ?: run {
                            completion.invoke(arrayListOf(), Throwable("Empty Body"))
                        }
                    } else {
                        completion.invoke(arrayListOf(), Throwable("NetworkError"))
                    }
                } ?: run {
                    completion.invoke(arrayListOf(), Throwable("No Response"))
                }
            }
        })
    }

    override fun buildSearchTerm(name: String?,
                                 brewedAfter: Date?,
                                 brewedBefore: Date?): HashMap<String, String> {

        val searchTerm = hashMapOf<String, String>()
        name?.let { searchTerm[QUERY_PARAM_BEER_NAME] = it }
        brewedAfter?.let { searchTerm[QUERY_PARAM_BREWED_AFTER] = dateFormatter.format(it) }
        brewedBefore?.let { searchTerm[QUERY_PARAM_BREWED_BEFORE] = dateFormatter.format(it) }

        return searchTerm
    }
}