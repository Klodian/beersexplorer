package com.kambo.klodian.beersexplorer.configurations

interface IEnvironmentVariables{
    val requestTimeout : Long
}