package com.kambo.klodian.beersexplorer.di

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
@ApplicationScope
class ContextModule(private val context: Context) {
    @Provides
    fun provideContext(): Context = context
}