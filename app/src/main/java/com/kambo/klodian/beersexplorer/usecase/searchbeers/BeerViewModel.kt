package com.kambo.klodian.beersexplorer.usecase.searchbeers

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.kambo.klodian.beersexplorer.model.beer.Beer
import com.kambo.klodian.beersexplorer.model.beer.IBeerModel
import com.kambo.klodian.beersexplorer.network.NetworkState
import com.kambo.klodian.beersexplorer.usecase.searchbeers.datasource.BeerDataSource
import com.kambo.klodian.beersexplorer.usecase.searchbeers.datasource.BeerDataSourceFactory
import java.util.*
import javax.inject.Inject


class BeerViewModel @Inject constructor(private val beerModel: IBeerModel,
                                        private val pagedListConfig: PagedList.Config) : ViewModel(), IBeerViewModel {

    private var storedBeforeDate: Date? = null
    private var storedAfterDate: Date? = null

    private var beersDataSourceFactory = BeerDataSourceFactory(beerModel, null)
    private var lastBeersFetchLiveData: LiveData<PagedList<Beer>> =
            LivePagedListBuilder<Int, Beer>(beersDataSourceFactory, pagedListConfig).build()

    override fun getDateFormat() = beerModel.dateFormat

    override fun getLastBeersLiveData() = lastBeersFetchLiveData

    override fun getNetworkState(): LiveData<NetworkState> {
        return Transformations
                .switchMap<BeerDataSource, NetworkState>(beersDataSourceFactory.beersDataSourceLiveData) { it.networkState }
    }

    override fun getBeersLiveData(name: String?,
                                  brewedAfter: Date?,
                                  brewedBefore: Date?): LiveData<PagedList<Beer>> {

        val searchParam = beerModel.buildSearchTerm(name, brewedAfter, brewedBefore)
        beersDataSourceFactory = BeerDataSourceFactory(beerModel, searchParam)
        lastBeersFetchLiveData = LivePagedListBuilder<Int, Beer>(beersDataSourceFactory, pagedListConfig).build()
        return lastBeersFetchLiveData
    }

    override fun storeBeforeDate(date: Date?) {
        this.storedBeforeDate = date
    }

    override fun getBeforeDate(): Date? {
        return this.storedBeforeDate
    }


    override fun storeAfterDate(date: Date?) {
        this.storedAfterDate = date
    }

    override fun getAfterDate(): Date? {
        return this.storedAfterDate
    }

}
